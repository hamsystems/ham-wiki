<!-- TITLE: Account Settings -->
<!-- SUBTITLE: A quick summary of Account Settings -->

# Account Settings
## Overview
1. Click the three lines button <img src="/uploads/account/3-lines.jpg" alt="3 Lines" class="inline"/>, located at the top left corner
2. Click “Account”

From here you can have an overview of your account
* how many rules you have used/in total
* how many groups you have used/in total
* how many floorplans you have used/in total
* how many SMS messages you have left

## Change password
1. Click “Change password” button
2. Enter your new password
3. Retype your new password
4. Check the “I’m not a robot” button and if required solve the puzzle to authenticate you are not a machine
5. Click "Submit" button

## Settings
### Preferred currency
1. Input your preferred currency unit

### Cost per kWh
1. Input the cost per kWh that you are charged by your energy provider

### Cost per kWh (alternate)
1. Input the alternate cost per kWh that you are charged by your energy provider

In order for the changes to take effect you have to click the “Update settings” button

## Delete account
1. Click “Delete account” button
2. Confirm that you want to delete your account by clicking "Ok" button