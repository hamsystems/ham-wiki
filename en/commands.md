<!-- TITLE: Commands -->

# Device Commands
On this section we can see the commands that can be sent to the device. Whitespace and punctuation is important

The device responses are in the format: `[{response_data},"ERROR"]`

`response_data` depends on the command and `"ERROR"` depends on the error encountered. Error is an empty string if there are no errors.


## Device state
```json
{
	"serialno":"14:3", // serialno
	"disconnected":false, // connected to server
	"updating":false, // is updating
	"id":"Lab Outlet", // friendly name
	"R":{"P":26,"E":5731552,"V":227,"I":307,"Q":54}, // Readings
	"E":[0], // Events
	"O":[255], // Outputs
	"family":"14", // Device family, first part of serialno
	"version":"0.7.29", // firmware version
	"T":[[0,0,0,0,0],[0,0,0,0,0],[0,0,0,0,0],[0,0,0,0,0],[0,0,0,0,0],[0,0,-1576074900,24,0]], // Schedules
	"uptime":255360, // Time in seconds since last device restart
	"ip":"192.168.10.3", // IP address
	"mac":"5c:cf:7f:ad:d7:8c", // MAC address
	"S":{"sS_AD":"hamsystems.eu","iS_PO":9002,"iALC":0}, // Device settings
	"untime":"1512565357.848" // Time in seconds since UNIX epoch in UTC
}
```

`R` is a dictionary with [device readings](#device_readings)
`O` is a list of [output values](#output_values). The index in the list is the index of the output, the output values are in the range `[0, 255]` and have different effects depending on the output type. For on/off outputs for example, `0` means off and `> 0` means on
`E` is a list of events. The index in the list is the index of the event. The values in the list is the unix timestamp of the last event.
`T` is a list of schedules.
`S` is a dictionary of device settings

### Device readings
`P`: Active power in Watts
`P_`: Aproximate Active power in Watts
`E`: Energy in Watt\*seconds, monotonic
`E2`: Energy in Watt\*seconds, Second counter (for nightly/seasonal billing), monotonic. Counter selection achieved with scheduling
`Q`: Reactive power in VAR
`V`: Voltage in Volts
`I`: Current in mA
`T`: Temperature in 0.01Celsius (2500 is 25 deg C)
`GI`: General digital input

### Output values
`on/off`: `0` is off `> 0` is on
`range`: output intensity in`v/255*100 %`
`thermostat temperature`:  the thermostat set point in Celcius
`thermostat mode`: Thermostat mode according to the mapping below
 
 - 0 : OFF
 - 1 : AUTO
 - 2 : HEAT
 - 3 : COOL
 
### Settings
`iALC`: allow local control allow HTTP commands on LAN
`iDMC`: disable manual control, any local manual output controls such as on/off buttons are ignored
`iPKW`: pulses per kWh as stated in the documentation of the kWh meter used
`OT0`: auto turn off output #0 timer in seconds, 0 for no auto-turn off

## READ
Format `READ`
Get device state readings(`R`)/outputs(`O`), `rssi`,`id`

Example:
`[{"untime":"1551704369.267","id":"HAM 1","rssi":"-58","R":{"E":66960000,"P_":0,"GI":1,"E2":0},"E":[0],"O":[255],"family":"12","serialno":"12:152","disconnected":false},""]`

## WRITE 
Format `WRITE index,value`
Set the ouput state at `index` to `value`
For example `WRITE 0,255`, sets output 0 to 255 (fully on)

## STAT
Format `STAT`
Get device info
`[{"serialno":"12:152","uptime":442364,"version":"0.8.39","hostname":"HAM 1","ip":"192.168.20.103","mac":"5c:cf:7f:ae:94:fe","untime":"1551707144.926","family":"12","disconnected":false},""]`

## TIMED
Format `TIMED index,start,interval,duration,output_index,output_value`
Set schedule
`index` is the index in the Schedule table
`start` is the start of schedule in UNIX timestamp (seconds) in UTC
`interval` the interval in seconds that the schedule repeats, 0 for no repeat. High bits set weekday mask
`duration` the duration of the schedule, after which the output will be set to 0
`output_index` the output to set the value to, negative for selecting energy counter
`output_value` the value to set the output to

## RTIMED
Format `RTIMED`
Read schedules
The schedules are read in an array of arrays where the index of the "T" array is the TIMED index, and each shedule array is in the format `[index,start,interval,duration,output_index,output_value]` as described above

## SET
Format `SET "key","value"`
Set [settings](#settings)

## RSET
Format `RSET`
Read settings