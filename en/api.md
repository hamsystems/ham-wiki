<!-- TITLE: API -->
<!-- SUBTITLE: Programmatic access to devices -->

# Introduction
Every hardware device has an serialno in the form `F:S` (e.g. `12:34`). The `F` part of serialno is the product family and it is common within similar hardware. For example, all DinSwitch devices have serialnos in the form `12:S`, whereas all Plugs have `14:S` and and so on. S is an incremental number unique for every device within the same family. The serialno of a device is the identifier used by the server, the UI and the API endpoints. Example serialnos are: `12:4`, `12:7`, `14:20`, `8:4`

For every device, two random keys are generated. One is `device_key` and it is the setup Access Point (AP) WPA password and it is also used to claim a device. The second key is a long sequence that the server uses to authenticate a device connection and the device uses to authenticate the server.
	
All communications and interactions with devices are made through the server by default. There is the option to send HTTP requests with device commands directly to devices on their IP address once they are connected on a LAN, but it is disabled by default for security reasons. The server authenticates the devices, authenticates the clients and passes commands and data between the two. The server handles datalogging and rule evaluation among other things.

# Authentication
Logged-in user is determined by the server, using a supplied `api_key` or an `access_token` via `COOKIE`, `POST` or `GET` parameters. In case, `access_token` or `api_key` are supplied multiple times, `api_key` has precedence over `access_token` and then, `GET` parameters have the highest priority, `POST` parameters are next and last are `COOKIE`

An access token is generated in OAuth2 fashion when a user logs-in using his email and password. Multiple `access_tokens` can be active up to a certain limit. When this limit is exceeded, the older `access_tokens` are replaced and invalidated. Access tokens also have an expiration date. This means that access tokens are temporary

A `api_key` on the other hand, never expires but every user account is limited to only one. You can generate an `api_key`, (which will replace the already existing one) by visiting the user account section in the HAM website and using the option generate API key


# /get_user_devices.php
URL: https://hamsystems.eu/get_user_devices.php

Returns user device information including floorplans, groups and allowances

Parameters:  [GET/POST]
- `access_token` or `api_key`

Response:
```json
{  
   "devices":[  
      {  
         "serialno":"12:0",
         "name":"Waterheater",
         "icon":"",
         "settings":{  
            "safe_outputs":false,
            "pin_code":"",
            "no_datalogging":false,
            "springy_outputs":false,
            "cost_per_kwh":0,
            "cost_per_kwh2":0,
            "gi_name":"\u0393\u03b5\u03bd. \u0395\u03af\u03c3.",
            "gi_closed":"\u03ba\u03bb\u03b5\u03b9\u03c3\u03c4\u03ae",
            "gi_open":"\u03b1\u03bd\u03bf\u03b9\u03ba\u03c4\u03ae",
            "gi2_name":"\u0393\u03b5\u03bd. \u0395\u03af\u03c3. 2",
            "gi2_closed":"\u03ba\u03bb\u03b5\u03b9\u03c3\u03c4\u03ae",
            "gi2_open":"\u03b1\u03bd\u03bf\u03b9\u03ba\u03c4\u03ae",
            "simple_reading":"",
            "summary_include":"default",
            "door_mode":false,
            "billing_start":0,
            "billing_interval":0,
            "fixed_cost":0,
            "billing_interval_unit":"months"
         },
         "floorplans":[  
							...
         ],
         "groups":[  
							...
         ],
         "features":{  
            "device":"12:0",
            "disaggregation":"1",
            "forecasting":"1"
         },
         "permissions":"2"
      },...],
   "messages":[  

   ],
   "user_id":0,
   "user_allowances":{  
      "group":{  
         "count":0,
         "allowed":1,
         "pass":true
      },
      "floorplan":{  
         "count":0,
         "allowed":3,
         "pass":true
      },
      "rule":{  
         "count":0,
         "allowed":1,
         "pass":true
      },
      "sms":0,
      "phonecalls":0
   }
}

```

# /device_command.php
URL: https://hamsystems.eu/device_command.php

Parameters [GET/POST]
- `access_token` or `api_key`for authentication
- `serialno` of the device
- `command` the [command](commands) to send to the device

Responses:
example for `serialno = 12:0`, `command = READ`

```json
{
   "messages": [], //API error messages
   "response": {
       "error": "",// device server error
       "data": [0, // command_id, always 0
           // device response
           [{
           "untime":"1551704369.267",
           "id":"HAM 1", 
           "rssi":"-58", 
           "R":{         
               "E":66960000,
               "P_":0,
               "GI":1,
               "E2":0
           },
           "E":[0],
           "O":[255],
           "family":"12",
           "serialno":"12:0",
           "disconnected":false
           },
           ""] // device command error
					 
       ]
   }
}
```

# Virtual devices
It is possible to create and update the state of non-HAM devices for purposes such as datalogging, non-HAM systems integrations and more. These kind of devices are referred to as  'virtual devices' and you can get access and update their state after contacting our company. Their serial numbers start with `v:`.

Each virtual device has a set number of readings that can be updated and datalogged, and these can be configured from the virtual device settings as shown below
![Virtual Device Settings](/uploads/virtual-device-settings.png "Virtual Device Settings")

There is an entry per reading/register. For each entry
- first field is the name of the reading
- second field is the multiplier by which the raw value must be mutliplied in order to convert to the standard units used by the system
- with the last field you select the type of the reading for the list of available readings

After you are happy with your settings, you can click on the Submit button

To update the state of the virtual device you have to use the api as described below:

# /virtual_device_update.php
URL: https://hamsystems.eu/virtual_device_update.php
Parameters [JSON]
- `access_token` or `api_key`for authentication
- `serialno` of the device
- `R` the readings in the device state format, refer to  [command](commands) for details

It is important to use "u#" for the readings, where # is the index of the reading in the order that is configured in the virtual device settings. 

Example request (v:x), is the serialno :
```json
{
	  "api_key": "YOUR_API_KEY",
	  "serialno": "v:1",
	  "R": {
		    "u0": 1200000,
	    	"u1": 1,
	    	"u2": 1,
		    "u3": 1,
	    	"u4": 1,
	    	"u5": 1
  	}
}

```
The response includes the "current" state of the device. Example response for the request above:

```json
{
    "messages": [],
    "response": {
        "error": "",
        "data": {
            "serialno": "v:1",
            "untime": "1558713914.902",
            "disconnected": false,
            "updating": false,
            "R": {
                "u0": 1200000,
                "u1": 1,
                "u2": 1,
                "u3": 1,
                "u4": 1,
                "u5": 1
            },
            "family": "v"
        }
    }
}

```