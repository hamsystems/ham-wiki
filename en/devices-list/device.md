<!-- TITLE: Device Menu-->
<!-- SUBTITLE: Device's individual information can be seen here -->

# Inside a Device
While being in the Devices menu click on the device you wish
![Click Device](/uploads/click-device.jpg "Click Device")

Click the button “MORE” below the device you want
![Click Device More](/uploads/click-device-more.jpg "Click Device More")

All the device's information can be seen here
![Device More 2](/uploads/device-inside/device-more-2.png "Device More 2")

Click the Power/Energy/Cost button to see the corresponding consumption graph
![Device Cost Month](/uploads/device-inside/device-cost-month.jpg "Device Cost Month")

Click the "Today"/"This Week"/"This Month"/"This Year" button to see the graph of the corresponding period of time
![Device Power Week 2](/uploads/device-inside/device-power-week-2.jpg "Device Power Week 2")

## Rename a device
1. Click the appropriate button ![Rename Button](/uploads/device-inside/rename-button.jpg "Rename Button")
2. Enter the name you wish for the device
3. Click the Tick button
![Rename Device 1](/uploads/device-inside/rename-device-1.jpg "Rename Device 1")

## Change device's icon
1. Click the appropriate button ![Rename Button](/uploads/device-inside/rename-button.jpg "Rename Button")
2. Pick the icon you wish for the device
3. Click the Tick button
![Change Icon 2](/uploads/device-inside/change-icon-2.jpg "Change Icon 2")

## Delete a device
1. Click the appropriate button ![Delete Device](/uploads/device-inside/delete-device.jpg "Delete Device")
2. Click "Ok" button to confirm

## Share a device
1. Click “Access and sharing”
![Share A Device](/uploads/device-inside/share-a-device.jpg "Share A Device")

2. Enter the email of the user you wish to share your device with
3. Grant the other user the permissions you wish on your device
4. Click the “Update sharing” button
![Share A Device 1](/uploads/sharing/share-a-device-1.png "Share A Device 1")

## Device settings
1. Click “Settings”
![Device Settings Button](/uploads/device-settings/device-settings-button.jpg "Device Settings Button")

### Avoid turning off/on a device (PIN code lock)
1. Enter the PIN code you wish in the text field provided

## Upgrade your device with the latest firmware
1. Click “Information”
![Info Device](/uploads/device-inside-info/info-device.jpg "Info Device")

2. Click the “Upgrade” button
![Upgrade Firmware](/uploads/device-inside-info/upgrade-firmware.jpg "Upgrade Firmware")

3. If you get a message that the upgrade did not succeed, then re-click “Upgrade” button until you get a confirmation that the device is upgraded

## Downgrade your device with the previous firmware
1. Click “Information”
![Info Device](/uploads/device-inside-info/info-device.jpg "Info Device")

2. Click the “Fallback” button
![Fallback Firmware](/uploads/device-inside-info/fallback-firmware.jpg "Fallback Firmware")
