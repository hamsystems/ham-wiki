<!-- TITLE: Claim A Device -->
<!-- SUBTITLE: A quick summary of Claim A Device -->

# Claim a device
1. Click the three lines button <img src="/uploads/account/3-lines.jpg" alt="3 Lines" class="inline"/>, located at the top left corner
2. Click “Claim a device” button

![Claim Button](/uploads/account/claim-button.jpg "Claim Button")

3. Insert the correct Device Key (D/K) number that can be found either on your purchased device or in your device manual

![Claim A Device 2](/uploads/account/claim-a-device-2.jpg "Claim A Device 2")

4. Click “Ok” button
