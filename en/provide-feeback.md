<!-- TITLE: Provide Feeback -->
<!-- SUBTITLE: A quick summary of Provide Feeback -->

# Provide Feedback / Report a bug
1. Click the three lines button <img src="/uploads/account/3-lines.jpg" alt="3 Lines" class="inline"/>, located at the top left corner
2. Click “Send feedback” button

<img src="/uploads/account/feedback-button.png" alt="Feedback Button" width="900px"/>{.align-center}

3. Write your message in the text field that is provided
4. Check the “I’m not a robot” button and if required solve the puzzle to authenticate you are not a machine

<img src="/uploads/account/feedback-send.jpg" alt="Feedback Send" width="900px"/>{.align-center}

5. Click “Send” button.
