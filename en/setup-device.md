<!-- TITLE: Setup Device -->
<!-- SUBTITLE: Setup device to connect to the Wi-Fi router -->

# Setup Device
In order for a new device to connect to the Wi-Fi router, you need to set it up to do so.
## using WPS
1. Click the three lines button <img src="/uploads/account/3-lines.jpg" alt="3 Lines" class="inline"/> located at the top left corner
2. Click “Setup device” button

<img src="/uploads/setup-device/setup-device.jpg" alt="Setup Device" width="600px"/>{.align-center}

3. Enable the WPS on your Wi-Fi router
4. Click "Next" button

<img src="/uploads/setup-device/setup-wps-next.jpg" alt="Setup Wps Next" width="600px"/>{.align-center}

5. Hold down the On/Off button on your device for a few seconds until the green light (status) starts flashing
6. Click "Next" button on the app

<img src="/uploads/setup-device/setup-devicebutton-next.jpg" alt="Setup Devicebutton Next" width="600px"/>{.align-center}

7. If everything went well, the device should now have the green light (status) lit, meaning that it is connected to the Wi-Fi router
8. Click "Done" button on the app

<img src="/uploads/setup-device/setup-done-2.jpg" alt="Setup Done 2" width="600px"/>{.align-center}

9. Note the device key (D/K) of your device and enter it on the next screen to register it. Click "Done" button

<img src="/uploads/setup-device/setup-done.jpg" alt="Setup Done" width="600px"/>{.align-center}

9a. If you have already claimed your new device then press the "Cancel" button on the popup window

<img src="/uploads/setup-device/setup-claim.jpg" alt="Setup Claim" width="600px"/>{.align-center}

9b. If you haven't already claimed your new device then enter the device key on the popup window and press "OK"

<img src="/uploads/setup-device/setup-claim-ok.jpg" alt="Setup Claim Ok" width="600px"/>{.align-center}

## via the HAM iOS app
1. Click the three lines button <img src="/uploads/account/3-lines.jpg" alt="3 Lines" class="inline"/>, located at the top left corner
2. Click “Setup device” button

<img src="/uploads/setup-ios/setup-device-ios.jpg" alt="Setup Device Ios" width="450px"/>{.align-center}

3. Click "I can't do WPS" button

<img src="/uploads/setup-ios/setup-wps-nowps-ios.jpg" alt="Setup Wps Nowps Ios" width="450px"/>{.align-center}

4. Hold down the On/Off button on your device for a few seconds until the green light (status) starts flashing
5. Click "Next" button on the app

<img src="/uploads/setup-ios/setup-devicebutton-next-ios.jpg" alt="Setup Devicebutton Next Ios" width="450px"/>{.align-center}

6. Connect manually to the Wi-Fi network that the device has created

<img src="/uploads/setup-ios/setup-choose-network-1.jpg" alt="Setup Choose Network 1" width="450px"/>{.align-center}

<img src="/uploads/setup-ios/setup-choose-network-2.jpg" alt="Setup Choose Network 2" width="450px"/>{.align-center}

7. After you are succesfully connected to the device's HAM network, click "Manual setup"

<img src="/uploads/setup-ios/setup-pick-manual-ios.jpg" alt="Setup Pick Manual Ios" width="450px"/>{.align-center}

8. Enter the SSID of your Wi-Fi network and its password

<img src="/uploads/setup-ios/setup-wifi-credentials-ios.jpg" alt="Setup Wifi Credentials Ios" width="450px"/>{.align-center}

9. You can also setup additional settings for your device by clicking the "Advanced" button

<img src="/uploads/setup-ios/setup-wifi-credentials-advanced-ios.jpg" alt="Setup Wifi Credentials Advanced Ios" width="450px"/>{.align-center}

10. If everything went well, the device should now have the green light (status) lit, meaning that it is connected to the Wi-Fi router
11. Click "Done" button on the app



12. Note the device key (D/K) of your device and enter it on the next screen to register it. Click "Done" button

<img src="/uploads/setup-ios/setup-done-ios.jpg" alt="Setup Done Ios" width="450px"/>{.align-center}

12a. If you have already claimed your new device then press the "Cancel" button on the popup window

<img src="/uploads/setup-ios/setup-claim-ios.jpg" alt="Setup Claim Ios" width="450px"/>{.align-center}

12b. If you haven't already claimed your new device then enter the device key on the popup window and press "OK"

<img src="/uploads/setup-ios/setup-claim-ok-ios.jpg" alt="Setup Claim Ok Ios" width="450px"/>{.align-center}


## via the HAM Android app
1. Click the three lines button <img src="/uploads/account/3-lines.jpg" alt="3 Lines" class="inline"/>, located at the top left corner
2. Click “Setup device” button

<img src="/uploads/setup-device/setup-device.jpg" alt="Setup Device" width="600px"/>{.align-center}

3. Click "I can't do WPS" button

<img src="/uploads/setup-device/setup-wps-nowps.jpg" alt="Setup Wps Nowps" width="600px"/>{.align-center}

4. Hold down the On/Off button on your device for a few seconds until the green light (status) starts flashing
5. Click "Next" button on the app

<img src="/uploads/setup-device/setup-devicebutton-next.jpg" alt="Setup Devicebutton Next" width="600px"/>{.align-center}

6. Disable the Mobile Data of your device
7. Click "Next" button on the app

<img src="/uploads/setup-device/setup-mobile-data-off.jpg" alt="Setup Mobile Data Off" width="600px"/>{.align-center}

8. Click the HAM Network that the HAM device has created

<img src="/uploads/setup-device/setup-pick-ham-network.jpg" alt="Setup Pick Ham Network" width="600px"/>{.align-center}

9. Enter the SSID of your Wi-Fi network and its password

<img src="/uploads/setup-device/setup-wifi-credentials.jpg" alt="SSetup Wifi Credentials" width="600px"/>{.align-center}

10. You can also setup additional settings for your device by clicking the "Advanced" button

<img src="/uploads/setup-device/setup-wifi-credentials-advanced.jpg" alt="Setup Wifi Credentials Advanced" width="600px"/>{.align-center}

11. If everything went well, the device should now have the green light (status) lit, meaning that it is connected to the Wi-Fi router
12. Click "Done" button on the app

<img src="/uploads/setup-device/setup-done-2.jpg" alt="Setup Done 2" width="600px"/>{.align-center}

13. Note the device key (D/K) of your device and enter it on the next screen to register it. Click "Done" button

<img src="/uploads/setup-device/setup-done.jpg" alt="Setup Done" width="600px"/>{.align-center}

13a. If you have already claimed your new device then press the "Cancel" button on the popup window

<img src="/uploads/setup-device/setup-claim.jpg" alt="Setup Claim" width="600px"/>{.align-center}

13b. If you haven't already claimed your new device then enter the device key on the popup window and press "OK"

<img src="/uploads/setup-device/setup-claim-ok.jpg" alt="Setup Claim Ok" width="600px"/>{.align-center}
