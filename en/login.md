<!-- TITLE: Login -->
<!-- SUBTITLE: A quick summary of Login -->

# Login
## Website using email
1. Click the “Sign in” button located at the top right corner of the website
2. Fill in your email and your password in the correct fields
3. Click the “Sign in” button.

## Website using Facebook account
1. Click the “Sign in” button located at the top right corner of the website
2. Click the “Facebook sign in” button

## App (iOS or Android) using email
1. Open HAM app on your Smartphone
2. Fill in your email and your password in the correct fields
3. Click the “Sign in” button.

## App (iOS or Android) using Facebook account
1. Open HAM app on your Smartphone
2. Click the “Facebook sign in” button