<!-- TITLE: Sign Up -->
<!-- SUBTITLE: A quick summary of Sign Up -->

# Sign up
## Website with email
1. Click the “Sign in” button located at the top right corner of the website
2. Fill in your email and your password in the correct fields
3. Check the “I’m not a robot” button and if required solve the puzzle to authenticate you are not a machine
4. Click the “Sign up” button

![Sign In 1](/uploads/sign-in-up/sign-in-1-.png "Sign In 1")

## App (iOS or Android) with email
1. Dowload the HAM app, both available in iOS and Android store
2. Open HAM app
3. Click the “Sign in” button
4. Fill in your email and your password in the correct fields
5. Check the “I’m not a robot” button and if required solve the puzzle to authenticate you are not a machine
6. Click the “Sign up” button
