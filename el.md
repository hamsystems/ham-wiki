<!-- TITLE: Οδηγίες -->
<!-- SUBTITLE: Οδηγίες στα Ελληνικα -->

# Περιεχόμενα
* [Εγγραφή](el/sign-up)
* [Είσοδος](el/login)
* [Ρυθμίσεις λογαριασμού](el/account-settings)
* [Αποστολή σχολίων](el/provide-feeback)
* [Καταχώρηση συσκευής](el/claim-a-device)
* [Ρύθμιση συσκευής](el/setup-device)
* [Λίστα συσκευών](el/devices-list)
	* [Συσκευή](el/devices-list/device/)
  * [Χρονοπρογραμματισμός](el/schedule)
* [Κατόψεις / Ομάδες συσκευών](el/floorplans)
* [Ανάλυση δεδομένων](el/data-analytics)
* [Κανόνες / Ειδοποιήσεις](el/rules)
* [Facebook chat bot](el/facebook)
* [Αποσύνδεση](el/sign-out)
