<!-- TITLE: Κανόνες Ειδοποιήσεις -->
<!-- SUBTITLE: Περιγραφή κανόνων Αν-Αυτό-Τότε-Αυτό -->

Για να μεταφερθείτε στους κανόνες επιλέξτε τη καρτέλα "Κανόνες"
<img src="/uploads/rules-el/rules-tab.png" alt="Rules tab" width="1200px"/>

# Δημιουργία κανόνα
1. Δημιουργία νέου κανόνα
<img src="/uploads/rules-el/rule-new.png" alt="New rule" width="1200px"/>

2. Καθορισμός κανόνα με βάση τις απαιτήσεις του χρήστη
<img src="/uploads/rules-el/rule.png" alt="Rule 1" width="1200px"/>

# Αλλαγή ονόματος κανόνα
1. Επιλογή του κουμπιού αλλαγής ονόματος
<img src="/uploads/rules-el/rule-rename.png" alt="Rule rename" width="1200px"/>

# Απενεργοποίηση κανόνα
1. Επιλογή του κουμπιού διαγραφής κανόνα
<img src="/uploads/rules-el/rule-off.png" alt="Rule off" width="1200px"/>

# Διαγραφή κανόνα
1. Επιλογή του κουμπιού διαγραφής κανόνα
<img src="/uploads/rules-el/rule-delete.png" alt="Rule delete" width="1200px"/>

2. Επιβεβαιώστε ότι θέλετε να διαγράψετε το κανόνα σας κάνοντας κλικ στο κουμπί "OΚ"

# Δημιουργία ειδοποιήσεων
## Ειδοποίηση μέσω της εφαρμογής
* Χρήση της επιλογής "Ειδοποιήστε με"
 <img src="/uploads/rules-el/notify-app.png" alt="Notify via app" width="1200px"/>

## Ειδοποίηση μέσω email
* Χρήση της επιλογής "Στείλτε μου email"
 <img src="/uploads/rules-el/notify-email.png" alt="Notify via email" width="1200px"/>

## Ειδοποίηση μέσω SMS
* Χρήση της επιλογής "Αποστολή SMS"
* Για να εμφανιστεί η επιλογή "Αποστολή SMS" πρέπει να έχετε διαθέσιμα SMS στο λογαριασμό σας
<img src="/uploads/rules-el/notify-sms.png" alt="Notify via SMS" width="1200px"/>

## Ειδοποίηση μέσω τηλεφωνικής κλήσης
* Χρήση της επιλογής "Τηλεφωνική κλήση"
* Για να εμφανιστεί η επιλογή "Τηλεφωνική κλήση" πρέπει να έχετε διαθέσιμες τηλεφωνικές κλήσεις στο λογαριασμό σας
<img src="/uploads/rules-el/notify-phone-call.png" alt="Notify via phone call" width="1200px"/>

## Ειδοποίηση μέσω Facebook chat
* Χρήση της επιλογής "Στείλτε μου στο Facebook"
* Για να λάβετε ειδοποιήσεις μέσω Facebook chat θα πρέπει να έχετε συγχρονίσει το λογαριασμό σας στην εφαρμογή με το λογαριασμό σας στο Facebook
 <img src="/uploads/rules-el/notify-facebook.png" alt="Notify via Facebook" width="1200px"/>


