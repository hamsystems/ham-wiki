<!-- TITLE: Facebook -->
<!-- SUBTITLE: Επικοινωνία με τις συσκευές μέσω Facebook Messenger -->

# Facebook chat bot
* Προκειμένου να μπορείτε να ελέγξετε τις συσκευές σας μέσω Facebook Messenger και να λαμβάνετε Facebook Messenger ειδοποιήσεις σχετικά την κατάσταση των συσκευών σας θα πρέπει να κάνετε κλικ στο κουμπί "Send to Messenger"
* Για να λειτουργήσει αυτή η επιλογή θα πρέπει να είστε ήδη συνδεδεμένος στο Facebook είτε μέσω της ιστοσελίδας του είτε μέσω της εφαρμογής του

<img src="/uploads/facebook-connect.png" alt="Facebook connect" />{.align-center}