<!-- TITLE: Κατόψεις / Ομάδες συσκευών -->
<!-- SUBTITLE: Περιγραφή κατόψεων και ομάδων συσκευών -->

Για την δημιουργία κατόψεων ή ομάδας συσκευών επιλέξτε τη καρτέλα "Ομάδες"

<img src="/uploads/groups-el/groups-tab.png" alt="Groups tab" />{.align-center}
# Κατόψεις
## Δημιουργία νέας κάτοψης
* Κάντε κλικ στο κουμπί δημιουργίας νέας κάτοψης

<img src="/uploads/floorplans-el/floorplan-new.png" alt="Floorplan new" />{.align-center}

## Επεξεργασία κάτοψης
* Για επεξεργασία της κάτοψης κάντε κλικ στο κουμπί επεξεργασίας

<img src="/uploads/floorplans-el/floorplan-edit.png" alt="Floorplan edit" />{.align-center}

### Αλλαγή ονόματος κάτοψης
* Για αλλαγή ονόματος της κάτοψης πληκτρολογήστε το επιθυμητό καινούριο όνομα στο αντίστοιχο κουτάκι

<img src="/uploads/floorplans-el/floorplan-edit-name.png" alt="Flooplan edit name" />{.align-center}

### Αλλαγή εικόνας κάτοψης
* Για αλλαγή εικόνας της κάτοψης κάντε κλικ στο κουμπί "Αλλαγή εικόνας"

<img src="/uploads/floorplans-el/floorplan-change-picture.png" alt="Flooplan change picture" />{.align-center}

### Τοποθέτηση συσκευών στην κάτοψη
* Για τοποθέτηση συσκευών εντός της κάτοψης απαιτούνται δύο βήματα
1. Κάντε κλικ πάνω στη συσκευή που σας ενδιαφέρει να τοποθετήσετε εντός της κάτοψης

<img src="/uploads/floorplans-el/floorplan-dragndrop.png" alt="Flooplan drag and drop1" />{.align-center}

2. Κάντε κλικ στο σημείο εντός της κάτοψης που σας ενδιαφέρει να τοποθετήσετε τη συσκευή

<img src="/uploads/floorplans-el/floorplan-dragndrop-2.png" alt="Flooplan drag and drop2" />{.align-center}

3. Αν επιθυμείτε να αλλάξε το σημείο που τοποθετήσατε τη συσκευή επαναλάβατε τα δύο προηγούμενα βήματα

<img src="/uploads/floorplans-el/floorplan-dragndrop-3.png" alt="Flooplan drag and drop3" />{.align-center}

### Ορισμός Γεωφράκτη
1.  Για ορισμό γεωφράκτη τσεκάρετε το τετραγωνάκι του γεωρφράκτη

<img src="/uploads/floorplans-el/floorplan-geofencing.png" alt="Flooplan geofencing" />{.align-center}

2. Η εισαγωγή του σημείου που σας ενδιαφέρει μπορεί να γίνει είτε:
* Εισάγοντας τις συντεταγμένες του σημείου που σας ενδιαφέρει

<img src="/uploads/floorplans-el/floorplan-geofencing-1.png" alt="Flooplan geofencing1" />{.align-center}

* Επιλέγοντας τον ορισμό με βάση το τωρινό στίγμα του κινητού σας

<img src="/uploads/floorplans-el/floorplan-geofencing-2.png" alt="Flooplan geofencing2" />{.align-center}

* Επιλέγοντας το σημείο μέσω του χάρτη

<img src="/uploads/floorplans-el/floorplan-geofencing-3.png" alt="Flooplan geofencing3" />{.align-center}

### Διαμοιρασμός κάτοψης με άλλο χρήστη
* Για διαμοιρασμό της κάτοψης με άλλον χρήστη κάντε κλικ στο κουμπί "Κοινή χρήση"

<img src="/uploads/floorplans-el/floorplan-share.png" alt="Flooplan share" />{.align-center}

* Εισάγεται το email του χρήστη με τον οποίο θέλετε να διαμοιραστείτε την κάτοψη και τα δικαιώματα που επιθυμείτε να του δώσετε

<img src="/uploads/floorplans-el/floorplan-share-email.png" alt="Flooplan share email" />{.align-center}

### Αποθήκευση αλλαγών
* Για αποθήκευση των αλλαγών που πραγματοποιήσατε κάντε κλικ στο κουμπί "Ανανέωση"

<img src="/uploads/floorplans-el/floorplan-update.png" alt="Floorplan update" />{.align-center}

### Διαγραφή κάτοψης
* Για διαγραφή της κάτοψης κάντε κλικ στο κουμπί διαγραφής και επιβεβαιώστε την διαγραφή

<img src="/uploads/floorplans-el/floorplan-delete.png" alt="Floorplan delete" />{.align-center}

# Ομάδες συσκευών
## Δημιουργία νέας ομάδας συσκευών
* Κάντε κλικ στο κουμπί δημιουργίας νέας ομάδας συσκευών

<img src="/uploads/groups-el/group-new.png" alt="Group new" />{.align-center}

## Έλεγχος ομάδας συσκευών
* Κάντε κλικ στο διακόπτη για ταυτόχρονο άνοιγμα/κλείσιμο όλων των συσκευών που ανήκουν στην ομάδα

<img src="/uploads/groups-el/group-control.png" alt="Group control" />{.align-center}

## Επεξεργασία ομάδας συσκευών
* Κάντε κλικ στο κουμπί "ΠΕΡΙΣΣΟΤΕΡΑ"

<img src="/uploads/groups-el/group-edit.png" alt="Group edit" />{.align-center}

### Αλλαγή ονόματος ομάδας συσκευών
* Κάντε κλικ στο κουμπί δημιουργίας νέας κάτοψης

<img src="/uploads/groups-el/group-change-name.png" alt="Group change name" />{.align-center}

### Προσθήκη συσκευής στην ομάδα συσκευών
* Επιλέξτε τη συσκευή που επιθυμείτε από τη λίστα διαθέσιμων συσκευών και κάντε κλικ στο κουμπί της προσθήκης

<img src="/uploads/groups-el/group-add-device.png" alt="Group add device" />{.align-center}

### Αφαίρεση συσκευής από την ομάδα συσκευών
* Κάντε κλικ στο κουμπί αφαίρεσης της συσκευής που επιθυμείτε από την ομάδα συσκευών και επιβεβαιώστε την επιλογή σας

<img src="/uploads/groups-el/group-delete-device.png" alt="Group delete device" />{.align-center}

### Διαμοιρασμός ομάδας συσκευών με άλλο χρήστη
* Για διαμοιρασμό της ομάδας συσκευών με άλλον χρήστη κάντε κλικ στο κουμπί "Κοινή χρήση"

<img src="/uploads/groups-el/group-share.png" alt="Group share" />{.align-center}

* Εισάγεται το email του χρήστη με τον οποίο θέλετε να διαμοιραστείτε την ομάδα συσκευών και τα δικαιώματα που επιθυμείτε να του δώσετε

<img src="/uploads/groups-el/group-share-email.png" alt="Group share email" />{.align-center}

* Κάντε κλικ στο κουμπί "Ενημέρωση κοινής χρήσης" για να επιβεβαιώσετε την επιλογή σας

<img src="/uploads/groups-el/group-share-email.png" alt="Group share email confirmation" />{.align-center}

### Διαγραφή ομάδας συσκευών
* Για διαγραφή της ομάδας συσκευών κάντε κλικ στο κουμπί διαγραφής και επιβεβαιώστε την διαγραφή

<img src="/uploads/groups-el/group-delete.png" alt="Group delete" />{.align-center}