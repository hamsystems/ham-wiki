<!-- TITLE: User Manual -->
<!-- SUBTITLE: Contents -->


# HAM Devices
[Overview](en/ham-devices/overview)
## DinSwitch
* [Description](en/ham-devices/dinswitch/description)
* [Combinations](en/ham-devices/dinswitch/combinations)
* [Projects](en/ham-devices/dinswitch/projects)

## Plug
* [Description](en/ham-devices/dinswitch/description)

## LightSwitch
* [Description](en/ham-devices/dinswitch/description)


# HAM Platform
* [Sing up](en/sign-up)
* [Login](en/login)
* [Account settings](en/account-settings)
* [Provide feedback](en/provide-feeback)
* [Claim a device](en/claim-a-device)
* [Setup device](en/setup-device)
* [Devices menu](en/devices-list)
	* [Device](en/devices-list/device/)
* [Floorplans](en/floorplans)
* [Group devices](en/group-devices)
* [Data analytics](en/data-analytics)
* [Rules](en/rules)
* [Notifications](en/notifications)
* [API](en/api)
* [Device commands](en/commands)