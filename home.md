<!-- TITLE: HAM Systems -->
<!-- SUBTITLE: Wiki for HAM devices and HAM platform -->

<img src="/uploads/logo-combo.png" alt="HAM Logo" class="pagelogo" />

[English](en/)
[Ελληνικά](el/)


- Home Automation and More Private Company
- Orfanidou 6, Thessaloniki 54626, Greece
- Email: contact@hamsystems.eu
- Tel: +30 2310-555256


Android app:
https://play.google.com/store/apps/details?id=com.ham.app

iOS app:
https://itunes.apple.com/us/app/ham-systems/id1262469559

Alexa skill:
https://www.amazon.com/Home-Automation-and-More-P-C/dp/B06XKZGP59

Facebook Messenger Chat Bot:
https://www.facebook.com/homeautomationandmoreai/

Facebook:
https://www.facebook.com/homeautomationandmore/

LinkedIn:
https://www.linkedin.com/company/home-automation-and-more/